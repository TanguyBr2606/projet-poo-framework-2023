@extends('layouts')

@section('title')
    {{ 'Accueil' }}
@endsection

@section('content')
    @if(session('message'))
        <div class="alert alert-primary text-center w-50 mx-auto mt-4">
            {{ session('message') }}
        </div>
    @endif

    @if($errors->any())
        @foreach($errors->all() as $error)
            <div class="alert alert-danger text-center w-50 mx-auto mt-4">
                {{ $error }}
            </div>
        @endforeach
    @endif
@endsection
