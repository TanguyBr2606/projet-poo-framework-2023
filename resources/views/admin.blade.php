@extends('layouts')

@section('title')
    {{'Admin'}}
@endsection

@section('content')

    @if(session('success'))
        <div class="alert alert-success text-center w-50 mx-auto mt-4">
            {{ session('success') }}
        </div>
    @endif

    @if($errors->any())
        @foreach($errors->all() as $error)
            <div class="alert alert-danger text-center w-50 mx-auto mt-4">
                {{ $error }}
            </div>
        @endforeach
    @endif

    <div class="w-auto mx-4">
        <h2 class="text-center my-5">Liste des utilisateurs</h2>
        <table class="table container">
            <thead>
            <tr>
                <th>Nom d'utilisateur</th>
                <th>Email</th>
                <th>Cr&eacute;ation</th>
                <th>Derni&egrave;re connexion</th>
                <th>Rôle</th>
                <th>Changement Rôle</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{{$user->username}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{date_format(new DateTime($user->created_at),' d-m-y H:i:s')}}</td>
                    <td>{{date_format(new DateTime($user->lastlogin),' d-m-y H:i:s')}}</td>
                    @if($user->role == 1)
                        <td>Étudiant</td>
                    @elseif($user->role == 2)
                        <td>Professeur</td>
                    @elseif($user->role == 3)
                        <td>Administrateur</td>
                    @else
                        <td>Visiteur</td>
                    @endif
                    <td>
                        @if($user->role == 3)
                            {{'Administrateur'}}
                        @else
                            <form action="{{ route('changeRole' , $user->id) }}" method="post">
                                @csrf
                                <select class="form-select form-select-sm" name="role">
                                    <option value="0" @if($user->role === 0)selected @endif>Visiteur</option>
                                    <option value="1" @if($user->role === 1)selected @endif>Étudiant</option>
                                    <option value="2" @if($user->role === 2)selected @endif>Professeur</option>
                                </select>
                                <button type="submit" class="btn btn-primary my-2">Mise à jour</button>
                            </form>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
