@extends('layouts')

@section('title')
    {{ 'Liste des cours' }}
@endsection

@section('content')
    <h2 class="text-center my-5">Liste des Cours</h2>
    <table class="table table-hover  container w-50 table-bordered">
        <thead>
        <tr>
            <th scope="col">Nom du cours</th>
            <th scope="col" class="text-end">Code du cours</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($course as $cours)
            <tr>
                <td>{{$cours->name}} </td>
                <td class="text-end">{{$cours->code}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
