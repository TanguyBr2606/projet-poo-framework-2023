<!doctype html>
<html lang="en">
<head>
    <meta name="author" content="Tanguy Brondelet">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title') | Projet POO & Framework 2023 </title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
</head>
<body>
<main>
    <header>
        <nav class="navbar">
            <div class="container-fluid justify-content-center">
                <ul class="nav nav-tabs">
                    <li class="nav-item"><a href="{{ route('home') }}" class="nav-link">Accueil</a></li>
                    <li class="nav-item"><a href="{{ route('course') }}" class="nav-link">Liste des cours</a></li>
                    @guest()
                        <li class="nav-item"><a href="{{ route('login') }}" class="nav-link"> Login</a></li>
                    @else
                        @if(Auth::user()->role == 3)
                            <li class="nav-item"><a href="{{ route('admin') }}" class="nav-link"> Admin</a></li>
                        @endif
                        @auth()
                            <li class="nav-item"><a href="{{ route('profile', ['id' => Auth::user()->id]) }}"
                                                    class="nav-link">Profil</a></li>
                        @endauth
                        <li class="nav-item"><a href="{{ route('logout') }}"
                                                class="nav-link  text-danger">Logout</a></li>
                    @endguest
                </ul>
            </div>
        </nav>
    </header>
    @yield('content')
</main>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL"
        crossorigin="anonymous"></script>
</body>
</html>
