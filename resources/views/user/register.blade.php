@extends('layouts')

@section('title')
    {{ 'Inscription' }}
@endsection

@section('content')
    @if(session('success'))
        <div class="alert alert-success text-center w-50 mx-auto mt-4">
            {{ session('success') }}
        </div>
    @endif

    @if($errors->any())
        @foreach($errors->all() as $error)
            <div class="alert alert-danger text-center w-50 mx-auto mt-4">
                {{ $error }}
            </div>
        @endforeach
    @endif

    <div class="container w-25 mx-auto">
        <h1 class="text-center my-5">Création du Profil</h1>
        <form action="{{ route('createProfile') }}" method="post">
            @csrf
            <label for="login" class="mt-2 form-label">Identifiant</label>
            <input type="text" id="login" name="username" class="form-control">
            <label for="email" class="mt-2 form-label">Email</label>
            <input type="email" id="email" name="email" class="form-control">
            <label for="pwd" class="mt-2 form-label">Mot de passe</label>
            <input type="password" id="pwd" name="password" class="form-control">
            <input type="submit" class="mt-3 btn btn-primary" style="margin-left: 37.5%" value="S'inscrire">
        </form>
    </div>
@endsection
