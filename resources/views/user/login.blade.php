@extends('layouts')

@section('title')
    {{ 'Login' }}
@endsection

@section('content')
    @if(session('success'))
        <div class="alert alert-success text-center w-50 mx-auto mt-4">
            {{ session('success') }}
        </div>
    @endif

    @if($errors->any())
        @foreach($errors->all() as $error)
            <div class="alert alert-danger text-center w-50 mx-auto mt-4">
                {{ $error }}
            </div>
        @endforeach
    @endif
    <div class="container w-25 mx-auto">
        <h1 class="text-center my-5">Login</h1>
        <form action="{{ route('connect')  }}" method="post">
            @csrf
            <input type="text" id="login" name="username" class="mt-3 form-control" placeholder="Identifiant">
            <input type="password" id="pwd" name="password" class="mt-3 form-control" placeholder="Mot de passe">
            <input type="submit" class="mt-3 btn btn-primary" style="margin-left: 40%" value="Connexion">
        </form>
        <br>
        <p class="text-center">Pas encore de compte alors, cliquez sur le bouton ci-dessous...</p>
        <form action="{{ route('register') }}" method="get">
            <button class="mt-2 btn btn-secondary text-center" style="margin-left: 35%">Nouveau compte</button>
        </form>
    </div>
@endsection
