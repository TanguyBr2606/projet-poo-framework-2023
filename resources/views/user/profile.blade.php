@extends('layouts')

@section('title')
    {{'Mon profil'}}
@endsection

@section('content')
    @if(session('message'))
        <div class="alert alert-primary text-center w-50 mx-auto mt-4">
            {{ session('message') }}
        </div>
    @endif

    @if($errors->any())
        @foreach($errors->all() as $error)
            <div class="alert alert-danger text-center w-50 mx-auto mt-4">
                {{ $error }}
            </div>
        @endforeach
    @endif
    <div class=" container my-5">
        <h2 class="text-center my-5">Profil</h2>
        <table class="table container table-striped">
            <tbody>
            <tr>
                <th>Nom d'utilisateur</th>
                <td>{{$user->username}}</td>
            </tr>
            <tr>
                <th>Email</th>
                <td>{{$user->email}}</td>
            </tr>
            <tr>
                <th>Cr&eacute;ation</th>
                <td>{{date_format(new DateTime($user->created_at),' d-m-y H:i:s')}}</td>
            </tr>
            <tr>
                <th>Derni&egrave;re connexion</th>
                <td>{{date_format(new DateTime($user->lastlogin),' d-m-y H:i:s')}}</td>
            </tr>
            <tr>
                <th>Rôle</th>
                @if($user->role == 1)
                    <td>Étudiant</td>
                @elseif($user->role == 2)
                    <td>Professeur</td>
                @elseif($user->role == 3)
                    <td>Administrateur</td>
                @else
                    <td>Visiteur</td>
                @endif
            </tr>
            </tbody>
        </table>
    </div>
    {{-- Boutons d'export des données du User en JSON--}}
    <div class="d-flex justify-content-center">
        <form action="{{ route('exportJSON')  }}" method="get">
            <button type="submit" class="btn btn-outline-success me-5">
                <i class="bi bi-download"></i>Export JSON
            </button>
        </form>
    </div>


    {{-- Formulaire de Mise à jour du Profil --}}
    <div class="container my-5">
        <h3><a href="#Update" data-bs-toggle="collapse" role="button"
               class="text-decoration-none btn btn-secondary dropdown-toggle">Mise &agrave; jour du profil</a>
        </h3>
        <div class="collapse" id="Update">
            <form action="{{ route('updateProfile', ['id' => Auth::user()->id] ) }}" method="post"
                  enctype="multipart/form-data">
                @csrf
                <div class="mb-3">
                    <label for="uu-email" class="mt-2 form-label">Email</label>
                    <input type="email" id="uu-email" name="email" class="form-control" value="{{Auth::user()->email}}">
                </div>
                <div class="mb-3">
                    <label for="uu-pwd" class="mt-2 form-label"> Mot de passe </label>
                    <input type="password" id="uu-pwd" name="password" class="form-control">
                </div>
                <button type="submit" class="btn btn-outline-primary">Soumettre</button>
            </form>
        </div>
    </div>

@endsection
