# Evaluation certificative POO - Framework 2023

## Prérequis

Vous devez avoir quelques prérequis suivants pour exécuter ce projet :

    - Un serveur Web (Ex.: Wamp)
    - PHP 8.2.0
    - MySql 8.0.33
    - Git
    - Composer

## Installation du projet dans le www

- Ouvrez votre git bash, accéder à votre répertoire _www_ de votre serveur en utilisant la commande **_cd_**
- Cloner le projet à l'aide de la commande _**git clone**_ le lien
  gitLab:  `git clone https://gitlab.com/TanguyBr2606/projet-poo-framework-2023`

## Installation de Laravel

### Commande installation du projet

Il vous faudra pour le bon fonctionnement du projet, que vous utilisiez les commandes ci-dessous dans _l'invite de
commande_ dans l'ordre donnée :

1. La commande `Composer Install` permettant d'installer les dépendances de laravel comme le dossier vendor.
2. La commande `php artisan installproject` qui vous permettra d'installer le projet en remplissant les
   champs demandés lors de son exécution qui permettra de créer le fichier .env qui personnalise les variables
   d'environnement de laravel.
3. Pour la création de la base de donnée, vous devrez aller sur _PHPMyAdmin_ et créé la base de donnée en appelant
   comme lors de l'installation et prendre le format utf8mb4_general_ci.

   **Attention**, le nom de la base de données doit être identique lors des deux demandes (nom DB lors connexion à
   celle-ci (point 2) et Création DB (point 3)).

4. La commande `php artisan dbMigration` permet de faire la migration des tables et d'insérer les données des cours.
   S'il vous demande si vous êtes sûr exécuter cette commande vous tapez _yes_.
5. Pour créer le compte administrateur, vous devriez exécuter la commande `php artisan createAdmin ` et remplir les
   champs demandés.

## Accès au projet

Deux possibilités :

### 1. L'accès avec le serveur Wamp et l'url

Pour se rendre sur l'url du projet en inscrivant dans la barre de recherche de votre navigateur :
`localhost/projetPOO2023/public/`

### 2. Accès par le serveur de Laravel

Vous pouvez accéder aussi en utilisant la commande `php artisan serve` et utilisant l'adresse `127.0.0.1:8000` .

## Informations supplémentaires

### Fonctionnalités utilisateur de base

- La consultation de la liste des cours est disponible sans être inscrit.

- Les utilisateurs qui s'inscriront, auront le rôle de _visiteur_ au départ (cf Rôle).
    - Ils auront accès à leur profil, pourront modifier leur email et leur mot de passe et exporter leur donnée dans un
      fichier JSON.

### Fonctionnalités du compte administrateur

L'administrateur aura les fonctionnalités de base d'un utilisateur (accès profil, modification email et mot de passe) et
pourra modifier le rôle des utilisateurs (cf. Rôle).

### Rôle

Les rôles sont indiqués dans la base de données à l'aide d'un entier, ils sont indiqués comme suit:

- 0 => Visiteur est le rôle par défaut
- 1 => Étudiant est le rôle validé par l'administrateur permettant à l'utilisateur de s'inscrire à un cours.
- 2 => Professeur est le rôle validé par l'administrateur permettant d'être assigné comme professeur d'un cours.
- 3 => Administrateur est un rôlé créé uniquement à l'installation du projet (cf. Commande point 3)

Les rôles Étudiant et Professeur ne sont là pour l'instant à titre indicatif, leurs fonctionnalités seront implémentés
dans un second temps.


