<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::namespace('App\Http\Controllers')->group(function () {
    // Main Page
    Route::get('/', function () {
        return view('home');
    })->name('home');

    // ListCourse Page
    Route::get('course', 'CourseController@course')->name('course');

    // Login Page
    Route::get('login', 'ConnectionController@login')->name('login');
    Route::post('login', 'ConnectionController@Connect')->name('connect');

    // Logout
    Route::get('logout', 'ConnectionController@logout')->name('logout');

    // Register Page
    Route::get('register', 'UserController@registerView')->name('register');
    Route::post('register', 'UserController@createProfile')->name('createProfile');

    // Profile Page - UpdatingProfile
    Route::middleware(['auth'])->group(function () {
        Route::get('profile/{id}', 'UserController@profileView')->name('profile');
        Route::post('updateProfile/{id}', 'UserController@updateProfile')->name('updateProfile');
    });

    // Export UserData
    Route::get('export/json', 'UserController@exportJSON')->name('exportJSON');

    // Admin Page
    Route::middleware(['admin'])->group(function () {
        Route::get('admin', 'AdminController@userList')->name('admin');
    });
    Route::post('changerole/{id}', 'AdminController@changeRole')->name('changeRole');
});

