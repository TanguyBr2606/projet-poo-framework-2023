<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class dbCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dbCreate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Database Creation';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        // Données demandées en console
        $schemaName = trim($this->ask('Enter the name of DATABASE to create'));

        // Configuration du charset qui spécifie le jeu de caractères
        $charset = config('database.connections.mysql.charset', 'utf8mb4');

        // Configuration collation qui spécifie comment les donneés sont traitées
        $collation = config('database.connections.mysql.collation', 'utf8mb4_general_ci');

        $query = "DROP DATABASE IF EXISTS $schemaName;";
        DB::statement($query);

        $query = "CREATE DATABASE IF NOT EXISTS $schemaName CHARACTER SET $charset COLLATE $collation;";
        DB::statement($query);

        echo "Database $schemaName created successfully !";
    }
}
