<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class CreateAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CreateAdmin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creation User Admin';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $validation = false;

        while (!$validation) {
            // Données demandées en console
            $username = $this->ask('Enter the admin name\'s (Enter for default)', 'admin');
            $email    = $this->ask('Enter the admin email');
            $password = $this->secret('Enter the admin password (min:5 characters)');

            // Vérification si c'est Valide par rapport aux règles
            $validator = Validator::make([
                'username' => $username,
                'email' => $email,
                'password' => $password,
            ], [
                'username' => 'required|string|min:4|max:50|unique:user',
                'email' => 'required|string|email|unique:user',
                'password' => 'required|string|min:5'
            ]);

            // Vérification si la validation a échoué
            if ($validator->fails()) {
                $this->error('Validation has failed !!');
                foreach ($validator->errors()->all() as $error) {
                    $this->error(' - ', $error);
                }
            } else {
                $validation = true;
            }
        }

        // Vérification si un admin existe déjà ou pas
        $admin = User::where(['role' => 3])->first();

        if (!$admin) {
            // Si pas d'admin. Création de l'admin avec son role
            $admin = User::create(['username' => strtolower($username),
                                   'email' => $email,
                                   'password' => Hash::make($password),
                                   'lastlogin' => now(),
                                   'role' => '3']);
            $this->info('The Admin user has created !');
        } else {
            // Si admin en DB, création d'un utilisateur normal (role => visiteur)
            $user = User::create(['username' => strtolower($username),
                                  'email' => $email,
                                  'password' => Hash::make($password),
                                  'lastlogin' => now(),
                                  'role' => '0']);
            $this->info('It\'s a normal user has been created, because an administrator already exists !');
        }
    }
}
