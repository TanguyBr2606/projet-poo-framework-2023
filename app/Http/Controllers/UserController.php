<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfileRequest;
use App\Http\Requests\RegisterRequest;
use App\Models\User;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;

class UserController extends Controller
{
    // Register View & CreateProfile
    /**
     * Méthode affichant la vue de création d'utilisateur
     *
     * @return \Illuminate\Contracts\View\View|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\Foundation\Application
     */
    public function registerView(): \Illuminate\Contracts\View\View|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\Foundation\Application
    {
        return view('user/register');
    }

    /**
     * Méthode permettant la création de l'utilisateur
     *
     * @param RegisterRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createProfile(RegisterRequest $request): \Illuminate\Http\RedirectResponse
    {
        // Création de l'utilisateur et envoie en DB avec la méthode Create()
        $user = User::create([
            'username' => strtolower($request->input('username')),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
            'lastlogin' => now(),
            'role' => '0'
        ]);

        return redirect()->route('login')->with('success', 'Inscription réussie !');
    }

    // Profile View + Update + ExportJson

    /**
     * Méthode affichant la vue du profil
     *
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
     */
    public function profileView($id): \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        $user = Auth::user();

        // Vérification de l'id de l'utilisateur par rapport à celui de l'id du user connecté
        if ($id != $user->id) {
            return back()->withErrors('Vous n\'avez pas la permission d\'accéder ce profil !!');
        }

        return view('user/profile', ['user' => $user]);
    }

    /**
     * Méthode permettant de mettre à jour l'email et le mot de passe de l'utilisateur
     *
     * @param ProfileRequest $request
     * @param                $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateProfile(ProfileRequest $request, $id): \Illuminate\Http\RedirectResponse
    {
        // Récupération de l'utilisateur connecté
        $user = Auth::user();

        // Vérification de l'id de l'utilisateur par rapport à celui de l'id connecté
        if ($id != $user->id) {
            return back()->withErrors('Vous n\'avez pas la permission de modifier ce profil !!');
        }

        // Vérification si c'est un nouveau email et différente de celui en DB
        if ($request->filled('email') && $request->input('email') != $user->email) {
            $user->email = $request->input('email');
        }

        // Vérification s'il y a un nouveau mot de passe
        if ($request->filled('password')) {
            $user->password = Hash::make($request->input('password'));
        }

        // Détermine s'il y a eu une modification ou pas avec la méthode isDirty()
        if ($user->isDirty()) {
            $user->save();
            return back()->with('message', 'Modification réussie !');
        } else {
            return back()->with('message', 'Aucune modification n\'a été effectuée !! ');
        }
    }

    /**
     * Méthode permettant d'exporter les données de l'utilisateur en format JSON
     *
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function exportJSON(): \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
    {
        if (Auth::check()) {
            $user = Auth::user();

            if ($user->role == 1) {
                $user->role = 'Étudiant';
            } elseif ($user->role == 2) {
                $user->role = 'Professeur';
            } elseif ($user->role == 3) {
                $user->role = 'Administrateur';
            } else {
                $user->role = 'Visiteur';
            }

            $data = [
                'id' => $user->id,
                'username' => $user->username,
                'email' => $user->email,
                'role' => $user->role,
                'created_at' => date_format(new DateTime($user->created_at), ' d-m-y H:i:s'),
                'lastlogin' => date_format(new DateTime($user->lastlogin), ' d-m-y H:i:s'),
            ];

            $jsonContent = json_encode($data, JSON_PRETTY_PRINT);
            $filename    = 'User-' . $user->id . '_' . time() . '.json';

            return Response::make($jsonContent, 200, [
                'Content-Type' => 'application/json',
                'Content-Disposition' => "attachement; filename=$filename"
            ]);
        }
        return back()->withErrors(['L\'export n\'a pas eu lieu !']);
    }
}
