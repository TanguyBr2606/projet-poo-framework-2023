<?php

namespace App\Http\Controllers;

use App\Http\Requests\AdminRequest;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class AdminController extends Controller
{
    /**
     * Méthode affichant les données des utilisateurs
     *
     * @return \Illuminate\Contracts\View\View|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\Foundation\Application
     */
    public function userList(): \Illuminate\Contracts\View\View|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\Foundation\Application
    {
        $users = user::all();
        return view('admin', ['users' => $users]);
    }

    /**
     * Méthode permettant de changer le rôle de l'utilisateur sauf celui qui est administrateur
     *
     * @param AdminRequest $request
     * @param              $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changeRole(AdminRequest $request, $id): \Illuminate\Http\RedirectResponse
    {
        // Récupération de l'utilisateur à mettre à jour
        $user = User::findOrfail($id);

        // Vérification si le role est différent de celui en DB
        if ($request->input('role') !== $user->role) {
            // Modification du rôle
            $user->role = $request->input('role');
            $user->save();
            return back()->with('success', 'Rôle de l\'utilisateur ' . $user->username . ' a été mis à jour avec succès !');
        } else {
            return back()->withErrors('Aucune modification de rôle n\'a eu lieu !');
        }
    }
}
