<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ConnectionController extends Controller
{
    // Login
    /**
     * @return \Illuminate\Contracts\View\View|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\Foundation\Application
     */
    public function login(): \Illuminate\Contracts\View\View|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\Foundation\Application
    {
        return view('user/login');
    }

    /**
     * @param LoginRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function connect(LoginRequest $request): \Illuminate\Http\RedirectResponse
    {
        // Valide les données fournies au formulaire de login
        $data = $request->validated();

        // Met le champ username en minuscules encas qu'il a été encodé en majuscules
        $data['username'] = strtolower($data['username']);

        // Connexion si les données fournies sont valides
        if (Auth::attempt($data)) {
            // Régénération de la session par sécurité pour ne pas utiliser une précédente
            $request->session()->regenerate();

            // Mise à jour de la colonne lastlogin
            if (Auth::check()) {
                Auth::user()->update(['lastlogin' => now()]);
            }

            return redirect()->route('profile', ['id' => Auth::id()])
                             ->with('message', 'Bienvenue ' . $data['username'] . ', vous êtes connectés.');
        } else {
            return back()->withErrors('Echec de l\'authentification');
        }
    }

    // Logout
    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    function logout(Request $request): \Illuminate\Http\RedirectResponse
    {
        Auth::logout();                    // Déconnecte l'utilisateur
        $request->session()->flush();      // Supprime les données stockées en session
        $request->session()->invalidate(); // Invalide la session
        return redirect('/')->with('message', 'Au revoir');
    }
}
