<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class AdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::user() && Auth::user()->role === 3;
    }

    /**
     * Get the validation that apply to the request.
     *
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'role' => 'required|in:0,1,2'
        ];
    }
}
