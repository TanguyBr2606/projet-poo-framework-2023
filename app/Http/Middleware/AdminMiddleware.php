<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminMiddleware
{
    /**
     * Méthode vérifiant si l'utilisateur est admin ou pas pour l'accès à la page ADMIN
     *
     * @param Request $request
     * @param Closure $next
     *
     * @return Response
     */
    public function handle(Request $request, closure $next): Response
    {
        if (auth()->check() && auth()->user()->role == 3) {
            return $next($request);
        }
        return back()->withErrors(['Vous n\'avez pas accès à cette partie !']);
    }
}
